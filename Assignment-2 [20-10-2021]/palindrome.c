#include <stdio.h>
#include <string.h>
#define MAX_LENGTH 100

int isPalindrome(const char *);

int main()
{
    char string[MAX_LENGTH];

    printf("Enter a string: ");
    scanf("%s", string);

    printf(isPalindrome(string) ? "%s is a palindrome\n" : "%s is not a palindrome\n", string);
    return 0;
}

int isPalindrome(const char *string)
{
    int len = strlen(string);
    
    for (int i = 0; len - i - 1 >= i ; i++)
        if (string[len - i - 1] != string[i])
            return 0;
    return 1;
}