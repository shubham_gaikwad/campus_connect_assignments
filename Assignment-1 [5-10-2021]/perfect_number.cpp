#include <iostream>
using namespace std;

bool isPerfect(long);

int main()
{
    int limit;

    cout << "Enter limit n: ";
    cin >> limit;

    for (int number = 1; number <= limit; number++)
        if (isPerfect(number))
            cout << number << " ";

    return 0;
}

bool isPerfect(long number)
{
    long sum = 0;

    for (long factor = 1; factor <= number / 2; factor++)
        if (number % factor == 0)
            sum += factor;


    if (number == sum)
        return true;
    return false;
}