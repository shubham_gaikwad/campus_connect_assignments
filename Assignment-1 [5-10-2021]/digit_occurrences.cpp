#include <iostream>
using namespace std;

int countOccurrences(const string, int);

int main()
{
    string number;
    int digit_count_array[10] = {0}, index;

    cout << "Enter number: ";
    cin >> number;

    cout << "Digit" << "\t" << "Occurrences" << endl;
    for (int digit = 0; digit < 10; ++digit)
        cout << digit << "\t" << countOccurrences(number, digit) << endl;

    /*//counting occurrences and storing it at index which represents the digit
    for (int i = 0; i < number.length(); i++)
    {
        index = (int)number[i] - '0';
        digit_count_array[index] += 1;
    }

    //displaying occurrences
    cout << "Digit"
         << "\t"
         << "Occurrences" << endl;
    for (int i = 0; i < 10; ++i)
        cout << i << "\t" << digit_count_array[i] << endl;
    */
    return 0;
}

int countOccurrences(const string number, int digit)
{
    int digit_counter = 0;

    for (int i = 0, num; i < number.length(); ++i)
    {
        num = (int)number[i] - '0';

        if (num == digit)
            digit_counter++;
    }

    return digit_counter;
}