#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100

typedef struct book
{
    char title[MAX];
    char author[MAX];
} Book;

typedef struct node
{
    Book book;
    struct node *next;
} BookList;

void addBook(BookList **bookList, Book book);
void viewBookList(BookList *bookList);
void searchBook(BookList *bookList, const char *title);
int deleteBook(BookList **bookList, const char *title);

int main()
{

    BookList *bookList = NULL;
    Book book;
    int choice;
    char title[MAX];

    do
    {
        printf("\nMENU: 0.EXIT 1.Add book 2.Search Book 3.Delete Book 4.View Book List\n");
        printf("Enter choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            printf("Enter book id, title, author: ");
            scanf("%s %s", book.title, book.author);
            addBook(&bookList, book);
            break;
        case 2:
            printf("Enter book title: ");
            scanf("%s", title);
            searchBook(bookList, title);
            break;
        case 3:
            printf("Enter book title: ");
            scanf("%s", title);
            deleteBook(&bookList, title);
            break;
        case 4:
            viewBookList(bookList);
            break;
        case 0 : break;
        default:
            printf("INVALID CHOICE\n");
            break;
        }
    } while (choice != 0);

    return 0;
}

void addBook(BookList **booklist, Book book)
{
    BookList *new = (BookList *)malloc(sizeof(BookList));
    BookList *last = *booklist;

    new->book = book;
    new->next = NULL;

    if (*booklist == NULL)
    {
        *booklist = new;
        return;
    }

    while (last->next != NULL)
        last = last->next;

    last->next = new;
    return;
}

void viewBookList(BookList *bookList)
{
    BookList *p = bookList;

    while (p != NULL)
    {
        printf("title: %s author: %s\n", p->book.title, p->book.author);
        p = p->next;
    }
}

void searchBook(BookList *bookList, const char *title)
{
    BookList *p = bookList;

    while (p != NULL)
    {
        if (strcmp(p->book.title, title) == 0)
        {
            printf("Book found!\n");
            return;
        }
        p = p->next;
    }
    printf("Book not found\n");
}

int deleteBook(BookList **bookList, const char *title)
{
    BookList *p = *bookList;

    while (p != NULL)
    {
        if (p->next == NULL)
        {
            if (strcmp(p->book.title, title) == 0)
                *bookList = NULL;
            return 1;
        }
        if (strcmp(p->next->book.title, title) == 0)
        {
            p->next = p->next->next;
            return 1;
        }
        p = p->next;
    }

    return 0;
}
