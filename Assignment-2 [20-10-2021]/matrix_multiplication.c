#include <stdio.h>
#define MAX 10

void multiplyMatrix(int[MAX][MAX], int[MAX][MAX], int[MAX][MAX], int, int, int);
void display2DArray(int[MAX][MAX], int, int);
void accept2DArray(int[MAX][MAX], int, int);

int main()
{
    int A[MAX][MAX], B[MAX][MAX], mul[MAX][MAX];
    int m, n, r, c;

    printf("Enter dim. of matrix A: ");
    scanf("%d%d", &m, &n);

    accept2DArray(A, m, n);
    display2DArray(A, m, n);

    printf("Enter dim. of matrix B: ");
    scanf("%d%d", &r, &c);
    accept2DArray(B, r, c);
    display2DArray(B, r, c);

    if (n != r)
    {
        printf("to multiply matrices no. of columns of one matrix should be equal to no. of rows of another matrix");
    }
    else
    {
        multiplyMatrix(mul, A, B, m, r, c);
        display2DArray(mul, m, c);
    }
    
    return 0;
}

void multiplyMatrix(int mul[MAX][MAX], int A[MAX][MAX], int B[MAX][MAX], int m, int r, int c)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < c; j++)
        {
            mul[i][j] = 0;
            for (int k = 0; k < r; k++)
            {
                mul[i][j] += A[i][k] * B[k][j];
            }
        }
    }
}

void display2DArray(int arr[MAX][MAX], int m, int n)
{
    for (int i = 0; i < m; i++)
    {
        printf("\n");
        for (int j = 0; j < n; j++)
        {
            printf("%d ", arr[i][j]);
        }
    }
    printf("\n");
}

void accept2DArray(int arr[MAX][MAX], int m, int n)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            printf("Enter value for A[%d][%d]:", i, j);
            scanf("%d", &arr[i][j]);
        }
    }
}
