#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100
#define MAP_SIZE 10

typedef struct book
{
    int id;
    char title[MAX];
    char author[MAX];
} Book;

Book *map[MAP_SIZE];

unsigned long hashcode(const char *string)
{
    unsigned long hash = 0;

    for (int i = 0; i < strlen(string); i++)
    {
        hash += string[i] * string[i];
        hash %= MAP_SIZE;
    }

    return hash;
}

void initMap()
{
    for (int i = 0; i < MAP_SIZE; i++)
        map[i] = NULL;
}

void viewBookList()
{
    for (int i = 0; i < MAP_SIZE; i++)
    {
        if (map[i] == NULL)
            printf("\t%i\t---\n", i);
        else
            printf("\t%i\t%s\n", i, map[i]->title);
    }
}

int addBook(Book *book)
{
    int index = hashcode(book->title);
    if (map[index] != NULL)
    {
        return 0;
    }
    map[index] = book;
    return 1;
}

Book *searchBook(char *title)
{
    int index = hashcode(title);
    if (map[index] != NULL && strcmp(title, map[index]->title) == 0)
    {
        return map[index];
    }
    return NULL;
}

Book *deleteBook(char *title)
{
    int index = hashcode(title);
    if (map[index] != NULL && strcmp(title, map[index]->title) == 0)
    {
        Book *deleted_book = map[index];
        map[index] = NULL;
        return deleted_book;
    }
    return NULL;
}


int main()
{
    int choice;
    Book *book;
    char title[MAX];

    initMap();

    do
    {
        printf("\nMENU: 0.EXIT 1.Add book 2.Search Book 3.Delete Book 4.View Book List\n");
        printf("\nEnter choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            book = (Book *)malloc(sizeof(Book));
            printf("\nEnter book title, author: ");
            scanf("%s", book->title);
            scanf("%s", book->author);
            if(addBook(book)) printf("\nBook added successfully\n");
            else printf("\nCouldn't add book\n");
            break;
        case 2:
            book = (Book *)malloc(sizeof(Book));
            printf("\nEnter book title: ");
            scanf("%s", title);
            book = searchBook(title);
            if (book == NULL)
                printf("\nBook not found\n");
            else
                printf("\nBook found - title: %s, author: %s\n", book->title, book->author);
            break;
        case 3:
            book = (Book *)malloc(sizeof(Book));
            printf("\nEnter book title: ");
            scanf("%s", title);
            book = deleteBook(title);
            if (book == NULL)
                printf("\nBook not found");
            else
                printf("\nBook deleted successfully\n");
            break;
        case 4:
            viewBookList();
            break;
        case 0:
            break;
        default:
            printf("INVALID CHOICE\n");
            break;
        }
    } while (choice != 0);
}

